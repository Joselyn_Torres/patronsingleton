﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSingleton
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Obtenemos la instancia por primera vez:
            Singleton primero = Singleton.ObtenInstanciaa();

            // Ahora ubicamos la instancia:
            primero.IngresarDatos("Joselyn", "Torres", 19);
            primero.Nobjeto();
            Console.WriteLine(primero);
            Console.WriteLine("*******************************************************************");

            // Si ubicamos una segunda instancia, se mostrará la anterior instancia:
            Singleton segunda = Singleton.ObtenInstanciaa();
            // Aquí podemos observar que es la misma instancia:
            Console.WriteLine(segunda);
            //Agregamos como decoración:
            Console.WriteLine("*******************************************************************");

            // Si ubicamos una tercera instancia, se mostrará la primera instancia:
            Singleton tercera = Singleton.ObtenInstanciaa();
            // Aquí podemos observar que es la misma instancia:
            Console.WriteLine(tercera);
            //Agregamos como decoración:
            Console.WriteLine("*******************************************************************");
        }
    }
}
