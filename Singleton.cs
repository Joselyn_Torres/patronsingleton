﻿namespace PSingleton
{
    public class Singleton
    {
        // Definición: El patrón de diseño Singleton Singleton es un patrón de diseño creacional,
        // que nos permite asegurarnos de que una clase tenga una única instancia, a la vez que
        // proporciona un punto de acceso global a dicha instancia.

        // Bueno, primero, creamos y guardamos la instancia:
        private static Singleton instanciaa;

        // Creamos variables propias de la clase Singleton:
        private string Nombre;
        private string Apellido;
        private int Edad;

        // Ahora creamos el constructor privado:
        private Singleton()
        {
            Nombre = "Sin asignar";
            Apellido = "Sin asignar";
            Edad = 19;
        }
        public static Singleton ObtenInstanciaa()
        {
            // Comprobamos si no existe la instancia:
            if (instanciaa == null)
            {
                // Si no existe la instancia, instanciamos:
                instanciaa = new Singleton();
                Console.WriteLine("*************** HOLA, ESTA ES LA PRIMERA INSTANCIA ****************");
            }
            // Returnamos la instancia:
            return instanciaa;
        }
        // Ahora creamos métodos propios de la clase Singleton:
        public override string ToString()
        {
            return string.Format("* La estudiante se llama {0} {1} y tiene la edad de {2} años.", Nombre, Apellido, Edad);
        }
        public void IngresarDatos(string inombres, string iapellido, int iedad)
        {
            Nombre = inombres;
            Apellido = iapellido;
            Edad = iedad;
        }
        // Este método representa cualquier objeto:
        public void Nobjeto()
        {
            Console.WriteLine("    {0} {1} es una estudiante de la carrera TI en la ULEAM.", Nombre, Apellido);
        }
    }
}
